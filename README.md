# CheetahHttp
用golang 实现的一个小型http框架 支持接口访问

```
func main() {

	hw := Cheetah.Hwdefault()
	hw.Get("/a", func(elp *Cheetah.Elp) {
		elp.RendenTemper("a.html")
	})
	hw.Post("/hello", func(elp *Cheetah.Elp) {
		elp.PrintString("hello word")
	})
	hw.Run("127.0.0.1", 1231)

}

```
