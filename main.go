package main

import Cheetah "Cheetah/http"

//Cheetah "Cheetah/http"

//实现一个http服务器
type Stu struct {
	Name string
}

func main() {

	hw := Cheetah.Hwdefault()
	hw.Get("/a", func(elp *Cheetah.Elp) {
		elp.Code = 200
		elp.RendenTemper("a.html")
	})
	hw.Get("/hello", func(elp *Cheetah.Elp) {
		a := Stu{Name: "zhangsan"}
		elp.Json(a)
		elp.PrintString("hello word")
	})
	hw.Run("127.0.0.1", 1231)

}
