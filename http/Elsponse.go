package Cheetah

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
)

type Elp struct {
	host       string
	data       []string
	ConentType string
	Code       int
}

func (e *Elp) Json(a interface{}) {
	jsons, _ := json.Marshal(a)
	fmt.Println(string(jsons))
	e.data = append(e.data, string(jsons))
}

func (e *Elp) RendenTemper(path string) {
	e.ConentType = "text/html"
	Html := ""

	buff := make([]byte, 1024)
	File, err := os.Open(path)
	if err != nil {
		fmt.Println("找不到路径")
		e.data = append(e.data, "错误！")
	} else {
		for {
			_, readerr := File.Read(buff)
			if readerr == io.EOF {
				break
			}
			Html += string(buff)
		}

		e.data = append(e.data, Html)
	}
}
func (e *Elp) PrintString(str string) {
	e.ConentType = "text/plain"
	e.data = append(e.data, str)
}
