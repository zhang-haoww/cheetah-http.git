package Cheetah

import (
	"fmt"
	"log"
	"net"
	"strings"
)

type GoHttp struct {
}

var DispObj DispController

func (hw GoHttp) Get(route string, call Callback) {
	useobj := Usercallback{}
	useobj.method = "GET"
	useobj.call = call
	DispObj.addController(route, useobj)
}

func (hw GoHttp) Post(route string, call Callback) {
	useobj := Usercallback{}
	useobj.method = "POST"
	useobj.call = call
	DispObj.addController(route, useobj)
}

func Hwdefault() GoHttp {
	return GoHttp{}
}

var CheLogo = `                                        
_|_|_|  _|                              _|                _|        
_|        _|_|_|      _|_|      _|_|    _|_|_|_|    _|_|_|  _|_|_|    
_|        _|    _|  _|_|_|_|  _|_|_|_|    _|      _|    _|  _|    _|  
_|        _|    _|  _|        _|          _|      _|    _|  _|    _|  
  _|_|_|  _|    _|    _|_|_|    _|_|_|      _|_|    _|_|_|  _|    _|  
                                                                      
                                                                      `

func (hw *GoHttp) Prinf() {
	fmt.Println(CheLogo)
}
func (hw GoHttp) Run(adderss string, port int) {
	hw.Prinf()
	var host = fmt.Sprintf("%s:%d", adderss, port)
	fmt.Println("http://" + host)
	sockerServer, err := net.Listen("tcp", host)
	if err != nil {
		log.Output(1, "当前地址已经被使用")
		panic(err)
	}
	for {
		conn, err := sockerServer.Accept()
		if err != nil {
			fmt.Print(err)
			continue
		}

		go dispServer(conn)
	}

}
func dispServer(con net.Conn) {
	defer con.Close()
	reviceByte := make([]byte, 512)
	n, _ := con.Read(reviceByte)

	if n == 0 {
		return
	}
	req := analysis(string(reviceByte[:n]))
	epl := Elp{}
	epl.host = req.Host

	callback := DispObj.GetCallback(req.Url, req.Method)

	callback.call(&epl)
	fmt.Println(req.Url, " ", con.RemoteAddr().String())
	//

	renders := render(epl, epl.ConentType)

	con.Write([]byte(renders))

}

//将响应流渲染为http协议
func render(elp Elp, ContentType string) string {
	ResponseHead := fmt.Sprintf("HTTP/1.1 %d OK", elp.Code)
	ResponseHead += "\r\n"
	ResponseHead += "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8"
	ResponseHead += "\r\n"
	ResponseHead += "Accept-Language:zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2"
	ResponseHead += "\r\n"
	ResponseHead += "Date: Thu, 07 Jun 2012 07:21:36 GMT"
	ResponseHead += "\r\n"
	ResponseHead += "Accept-Encoding:gzip, deflate"
	ResponseHead += "\r\n"
	ResponseHead += "Date: Thu, 07 Jun 2012 07:21:36 GMT"
	ResponseHead += "\r\n"
	ResponseHead += fmt.Sprintf("%s:%s", "Content-Type", ContentType)
	ResponseHead += "\r\n"
	ResponseHead += "gzip, deflate, br"
	ResponseHead += "\r\n"
	ResponseHead += "Server:yhWebServer"
	ResponseHead += "\r\n"
	ResponseHead += "Connection: close"
	ResponseHead += "\r\n"
	ResponseHead += "\r\n\r\n"
	for i := 0; i < len(elp.data); i++ {
		ResponseHead += elp.data[i]
	}
	return ResponseHead
}

type HttpRequest struct {
	Method     string
	Host       string
	Accept     string
	User_Agent string
	Url        string
	Headers    map[string]string
}

// 解析http协议
func analysis(str string) HttpRequest {

	httpRequestStr := strings.Split(str, "\r\n")
	reuqestobj := HttpRequest{}
	reuqestobj.Headers = map[string]string{}

	if len(strings.Split(httpRequestStr[0], " ")) > 1 {

		reuqestobj.Method = strings.Split(httpRequestStr[0], " ")[0]
		reuqestobj.Url = strings.Split(httpRequestStr[0], " ")[1]

	}
	fmt.Println(reuqestobj.Url)
	for i := 1; i < len(httpRequestStr); i++ {
		httptags := strings.Split(httpRequestStr[i], ":")

		if len(httptags) < 2 {
			continue
		}
		for j := 2; j < len(httptags); j++ {
			httptags[1] += ":" + httptags[j]
		}
		reuqestobj.Headers[httptags[0]] = httptags[1]
	}

	reuqestobj.Accept = reuqestobj.Headers["Accept"]
	reuqestobj.Host = reuqestobj.Headers["Host"]
	reuqestobj.User_Agent = reuqestobj.Headers["User-Agent"]
	return reuqestobj
}
